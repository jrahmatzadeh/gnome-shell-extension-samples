/**
 * GNOME Shell Extension Sample
 * 
 * Manage Windows on Current Workspace
 * 
 * LICENSE
 * Released Under MIT License.
 * 
 * AUTHOR
 * JustPerfection (c) 2020
 */
 
const Meta = imports.gi.Meta;

function init() {}

function enable() {
  
    let activeWorkspace = global.workspace_manager.get_active_workspace();
    let windows = activeWorkspace.list_windows();
  
    for (let i = 0; i < windows.length; ++i) {
  
        //windows[i].minimize();
        
        //windows[i].unminimize();
        
        //windows[i].maximize(
        //  Meta.MaximizeFlags.HORIZONTAL | Meta.MaximizeFlags.VERTICAL);
            
        //windows[i].move_resize_frame(true, 30, 30, 300, 300);
        
        windows[i].move_frame(true, 30, 30);
            
        log('Window Title: ' + windows[i].title);
    }
}

function disable() {}
