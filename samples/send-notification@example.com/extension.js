/**
 * GNOME Shell Extension Sample
 * 
 * Send Notification Message from GNOME Shell Extension
 * 
 * LICENSE
 * Released Under MIT License.
 * 
 * AUTHOR
 * JustPerfection (c) 2020
 */

const Main = imports.ui.main;

function init() {}

function enable() {
  Main.notify('Message Title', 'Message Body');
}

function disable() {}

