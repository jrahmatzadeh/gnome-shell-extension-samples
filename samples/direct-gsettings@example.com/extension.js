/**
 * GNOME Shell Extension Sample
 * 
 * Get or Set gsettings with direct schema id in GNOME Shell Background.
 * 
 * LICENSE
 * Released Under MIT License.
 * 
 * AUTHOR
 * JustPerfection (c) 2020
 */

const Gio = imports.gi.Gio;
let settings = new Gio.Settings({ schema: 'org.gnome.desktop.interface' });

function init() {
}

function enable() {
  // settings.set_string('gtk-theme', 'Yaru');
  log(settings.get_string('gtk-theme'));
}

function disable() {
}
