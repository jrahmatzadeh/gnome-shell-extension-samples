# GNOME Shell Extension Samples

Sample Extensions that can help GNOME Shell extension developers.

## How To Create GNOME Shell Extension

- [Textual Documentation](https://gitlab.com/justperfection.channel/how-to-create-a-gnome-extension-documentation)
- [Videos](https://www.youtube.com/playlist?list=PLr3kuDAFECjZhW-p56BoVB7SubdUHBVQT)

## Examples

- [Add Container To The Background](samples/add-container-to-the-background@example.com)
- [Add Container to Top Panel](samples/add-container-to-top-panel@example.com)  ([Tutorial Video](https://www.youtube.com/watch?v=iMyR5lJf7dU))
- [Animation](samples/animation@example.com)  ([Tutorial Video](https://www.youtube.com/watch?v=Lz3KRWYyKM4))
- [Direct gsettings](samples/direct-gsettings@example.com)
- [Drag and Drop](samples/drag-and-drop@example.com)  ([Tutorial Video](https://www.youtube.com/watch?v=2qVn6CjlDUQ))
- [Mainloop specific Time](samples/mainloop-specific-time@example.com)
- [Manage Current Workspace Windows](samples/manage-current-workspace-windows@example.com)
- [Move Container](samples/move-container@example.com)
- [Open and Close Apps](samples/open-close-apps@example.com)
- [Read and Write Files](samples/read-and-write-files@example.com)
- [Schemas Changed Signal](samples/schemas-changed-signal@example.com)
- [Send Notification](samples/send-notification@example.com)

## Rules for Contributing to This Repository

You can contribute to this repository by creating sample extensions. Please read [Rules](CONTRIBUTING.md) if you want to contribute.

## License

All files have been released under The MIT License.
