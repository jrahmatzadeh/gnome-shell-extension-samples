# Rules for Contributing to This Repository

- Each Example should be inside a folder. Folder name should be the same as extension `uuid` with this format: `sample-name@example.com`.

- Each example should do one thing and one thing right.

- The extension folder name should be self explanatory.

- Each extension should be install-able after copying to the GNOME Shell main extension folder (Just like a real extension).

- Add the main comment to top of the `extension.js` file like this:  
  
  ```js
  /**
  * GNOME Shell Extension Sample
  *
  * EXPLAIN_WHAT_THIS_CODE_DO
  *
  * LICENSE
  * Released Under MIT License.
  *
  * AUTHOR
  * YOUR_NAME (c) YEAR
  */
  ```

- Respect 80 characters margin.

- Avoid unnecessary comments. The code should be self explanatory.

- Avoid writing something that can be dropped. For example, you don’t need to use gsettings or gettext to show a text to the end user.

- Write a simple and readable code.

*This file may be updated in the future. Any change should be applied to all files in this repository.*
